var express = require("express");
var app = express();
var bodyParser = require("body-parser");

var connectToDb = require("./connectToDb.js");
var insertOrder = require("./insertOrder.js");

var url = "mongodb://localhost:27017/nabytok";

app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-type");
  next();
});

app.get("/drevo", function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");

  connectToDb(function(db) {
    var collection = db.collection("sklad");

    collection.findOne(function(err, doc) {
      res.json({
        drevo: doc.material
      });
    });
  });
});

app.get("/skrinky", function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");

  connectToDb(function(db) {
    var collection = db.collection("sklad");

    collection.findOne(function(err, doc) {
      res.json({
        skrinky: doc.skrinky
      });
    });
  });
});

app.get("/objednavky", function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  connectToDb(function(db) {
    var collection = db.collection("objednavky");

    collection.count(function(err, pocetObjednavok) {
      res.json({
        objednavky: pocetObjednavok
      });
    });
  });
});

app.get("/vybratObjednavku", function(req, res) {
  connectToDb(function(db) {
    db
      .collection("objednavky")
      .find()
      .toArray(function(err, objednavky) {
        res.json({
          objednavky: objednavky
        });
      });
  });
});

app.post("/objednavky", function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
  res.header("Access-Control-Allow-Headers", "Content-type");

  // ulozime objednavku do databazy
  connectToDb(function(db) {
    var collection = db.collection("objednavky");
    insertOrder(req.body, function(res) {
      console.log(res);
    });
  });
});

app.get("/objednavka/:id", function(req, res) {
  var id = req.params.id;

  connectToDb(function(db) {
    var objednavky = db.collection("objednavky");

    objednavky.findOne({ udaje: id }, function(err, doc) {
      if (!err) {
        res.json({
          success: true,
          data: doc
        });
      }
    });
  });
});

app.listen(3000);
