var connectToDb = require("./connectToDb.js");
var odobratSkrinkyZoSkladu = require("./odobratSkrinkyZoSkladu.js");
var odobratMaterialZoSkladu = require("./odobratMaterialZoSkladu.js");

module.exports = function(data, cb) {
	connectToDb(function(db) {
		var collection = db.collection("objednavky");

		collection.insertMany([data], function(err, res) {
			if (err) {
				console.log("chyba");
				console.log(err);
			}

			// odpocitame mnozstvo materialu a skriniek z toho co je na sklade
			odobratSkrinkyZoSkladu(data.skrinky, function(res) {});
			odobratMaterialZoSkladu(data.material, function(res) {});

			// vratit response z vkladu
			cb(res);
		});
	});
};
