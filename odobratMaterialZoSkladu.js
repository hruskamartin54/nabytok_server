var connectToDb = require("./connectToDb.js");

module.exports = function(mnozstvo, cb) {
	connectToDb(function(db) {
		var sklad = db.collection("sklad");

		sklad.findOne(function(err, doc) {
			sklad.findOneAndUpdate(
				{},
				{
					$set: {
						material: doc.material - mnozstvo
					}
				},
				function(err, res) {
					cb(res);
				}
			);
		});
	});
};
