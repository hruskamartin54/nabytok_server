const sinon = require("sinon");
const chai = require("chai");
const sinonChai = require("sinon-chai");
const expect = require("chai").expect;

var connectToDb = require("./../connectToDb.js");
var insertOrder = require("./../insertOrder.js");
var odobratSkrinkyZoSkladu = require("./../odobratSkrinkyZoSkladu.js");
var odobratMaterialZoSkladu = require("./../odobratMaterialZoSkladu.js");

before(function() {
  chai.use(sinonChai);
});

beforeEach(function() {
  this.sandbox = sinon.sandbox.create();
});

afterEach(function() {
  this.sandbox.restore();
});

describe("database connection", function() {
  it("connects to the database safely", function(done) {
    connectToDb(db => {
      expect(db).to.be.an("object");
      expect(db.databaseName).to.be.equal("nabytok");
      done();
    });
  });
});

describe("database insertion", function() {
  it("inserts order into database with correct data", function(done) {
    const data = {
      udaje: "Lorem Ipsum",
      adresa: "Letná 9",
      material: 200,
      dekor: "svetly",
      skrinky: 2
    };

    insertOrder(data, res => {
      const { ok, n } = res.result;
      const { ops } = res;

      expect(n).to.be.equal(1);
      expect(ok).to.be.equal(1);

      expect(ops.length).to.be.equal(1);
      expect(ops[0].udaje).to.be.equal("Lorem Ipsum");
      expect(ops[0].adresa).to.be.equal("Letná 9");
      expect(ops[0].material).to.be.equal(200);
      expect(ops[0].dekor).to.be.equal("svetly");
      expect(ops[0].skrinky).to.be.equal(2);

      done();
    });
  });
});

describe("material storage management", function() {
  it("subtracts material from the storage", function() {
    connectToDb(function(db) {
      const sklad = db.collection("sklad");

      sklad.findOne(function(err, doc) {
        const povodnyMaterial = doc.material;

        odobratMaterialZoSkladu(200, function(res) {
          expect(res.value).to.be.an("integer");
          expect(povodnyMaterial).to.be.equal(res.value);
        });
      });
    });
  });
});

describe("product storage management", function() {
  it("subtracts product from the storage", function() {
    connectToDb(function(db) {
      const sklad = db.collection("sklad");

      sklad.findOne(function(err, doc) {
        const povodnyMaterial = doc.skrinky;

        odobratSkrinkyZoSkladu(2, function(res) {
          expect(res.value).to.be.an("integer");
          expect(povodnyMaterial).to.be.equal(res.value);
        });
      });
    });
  });
});
