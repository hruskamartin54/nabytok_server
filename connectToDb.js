var MongoClient = require("mongodb").MongoClient;

var url = "mongodb://localhost:27017/nabytok";

module.exports = function(cb) {
	MongoClient.connect(url, function(err, db) {
		if (!err) cb(db);
	});
};
